import { Component, OnInit } from '@angular/core';
import { Attore } from '../entity/Attore';
import { AttoreService } from '../services/attore-service';

@Component({
  selector: 'app-modifica-attore',
  templateUrl: './modifica-attore.component.html',
  styleUrls: ['./modifica-attore.component.css']
})
export class ModificaAttoreComponent implements OnInit {
  
  attore:Attore
  constructor(private attoreService:AttoreService) { }
  
 
  ngOnInit(): void {
  }
  onSubmit(){
    this.attore=this.attoreService.form.value
    this.attoreService.putAttore(this.attore).subscribe(result=>{
      console.log(result)
    })
  }



  getInfoAttore(codAttore: number) {
  this.attoreService.getAttoriByID(codAttore).subscribe((result:Attore)=>{
      this.attore=new Attore(result.codAttore, result.nome,result.annoNascita,result.country)
      //inizializziamo il form
      this.attoreService.initializeFormGroup(this.attore)
    });
    
  }
}
