import { Component } from '@angular/core';
import{Router}from '@angular/router';
import{ModificaAttoreComponent} from '../modifica-attore/modifica-attore.component';
import{AttoreService}from '../services/attore-service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent {
  //attributi
  //Memorizzo il risultato
  result:any;
  keys: any;
  router:Router;
  attoriService:AttoreService;

  constructor(router:Router, attoreService:AttoreService){
    this.router=router;
    this.attoriService=attoreService;
   }

   //metodo per richiesta al server tramite attoreService
   visualizza(){
     this.attoriService.getAttori().subscribe((attori: any) =>
     {
       this.result = attori;
       this.keys=Object.keys(attori[0])
       console.log(this.result) ;
     });

   }
   performDelete(codAttore:number){
    this.attoriService.deleteAttore(codAttore);
   }

   performUpdate(id:number){
    this.router.navigate(['/putAttore']).then(() => {
      ModificaAttoreComponent.prototype.getInfoAttore(id);
    });
   }
}
