import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { InserimentoAttoreComponent } from './inserimento-attore/inserimento-attore.component';
import { ModificaAttoreComponent } from './modifica-attore/modifica-attore.component';
 
const routes: Routes = [
  {path:'home',component:HomeComponent},
 {path:'postAttore',component:InserimentoAttoreComponent},
  {path:'putAttore',component:ModificaAttoreComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
